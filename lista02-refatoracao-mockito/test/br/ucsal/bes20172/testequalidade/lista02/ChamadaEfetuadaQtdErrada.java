package br.ucsal.bes20172.testequalidade.lista02;

public class ChamadaEfetuadaQtdErrada extends Exception {
	private static final long serialVersionUID = 1L;

	public ChamadaEfetuadaQtdErrada(String message) {
		super(message);
	}
}
