package br.ucsal.bes20172.testequalidade.lista02;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AnotacoesExemplo {
	
	@BeforeClass
	public static void setupPorClasse(){
		System.out.println("\nexecuta uma vez antes do primeiro teste da classe atual rodar...");
	}
	
	@AfterClass
	public static void tearDownPorClasse(){
		System.out.println("\nexecuta uma vez depois do �ltimo teste da classe atual rodar...");
	}
	
	@Before
	public void setupPorTeste(){
		System.out.println("\nexecuta um vez antes de cada teste...");
		System.out.println("limpa uma certa �rea de dados");
		System.out.println("insere um conjunto de dados que � �til para cada um dos testes");
	}

	@After
	public void tearDownPorTeste(){
		System.out.println("executa um vez depois de cada teste...");
	}
	
	@Test
	public void teste1(){
		System.out.println("executa teste 1...");
	}
	
	@Test
	public void teste2(){
		System.out.println("executa teste 2...");
	}
	
	@Test
	public void teste3(){
		System.out.println("executa teste 3 ...");
	}
	
}
